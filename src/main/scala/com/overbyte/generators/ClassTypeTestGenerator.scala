package com.overbyte.generators

import com.overbyte.models.{BitField, ClassType, NumericField}

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class ClassTypeTestGenerator(classType: ClassType) extends Generator {
  val allBitFields: Seq[BitField] = {
    classType.fields.collect{ case NumericField(_, _, Some(bitFields)) =>
      bitFields
    }.flatten.filter(_.name != "unused")
  }

  def fieldParameters: String = {
    allBitFields.map { field =>
      val fieldType = if(field.width == 1) "Boolean" else "Int"
      s"${field.name}: $fieldType"
    }.mkString(", ")
  }

  def maskBits(size: Int): Int = {
    Integer.parseInt(Range(0, size).map(_ => "1").mkString, 2)
  }

  def generatorFor(field: BitField): String = {
    if(field.width == 1) "Gen.oneOf(false, true)" else s"Gen.choose(0, ${maskBits(field.width)})"
  }

  val defaultMapping = Map(
    "Long" -> "0L",
    "Double" -> "0.0",
    "Float" -> "0.0f",
    "Int" -> "0",
    "Short" -> "0",
    "Byte" -> "0",
    "String" -> "\"\""
  )

  def defaultArgs: String =
    classType.fields.map(field => defaultMapping(field.fieldType)).mkString(", ")

  override def generate: String = {
    val className = classType.name
    s"""
       |class ${className}Specification extends Properties("$className") {
       |  case class ${className}Struct($fieldParameters) {
       |    def write: $className = {
       |      val result = $className($defaultArgs)
       |${allBitFields.map(field => s"      result.${field.name} = ${field.name}").mkString("\n")}
       |      result
       |    }
       |
       |    def read(value: $className): ${className}Struct = {
       |      ${className}Struct(
       |${allBitFields.map(field => s"        ${field.name} = value.${field.name}").mkString(",\n")}
       |      )
       |    }
       |  }
       |
       |  def generate${className}Struct: Gen[${className}Struct] = for {
       |${allBitFields.map(field => s"    _${field.name} <- ${generatorFor(field)}").mkString("\n")}
       |  } yield ${className}Struct(${allBitFields.map(field => s"_${field.name}").mkString(", ")})
       |
       |  property("reversible") = forAll(generate${className}Struct){ struct =>
       |    struct == struct.read(struct.write)
       |  }
       |}
     """.stripMargin
  }
}
