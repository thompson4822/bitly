package com.overbyte.generators

import com.overbyte.models.{BitField, NumericField}

/**
  * Created by steve at OverByte on 7/2/16.
  */
case class NumericFieldGenerator(field: NumericField) extends Generator {
  // We only really care about the field if it is complex (broken into bit fields)
  override def generate: String = field.bitFields match {
    case Some(bitFields) =>
      s"""
         |${generateFields(bitFields).mkString("\n")}
       """.stripMargin
    case _ => ""
  }

  protected def generateFields(bitFields: Seq[BitField]): Seq[String] = {
    def recGenerateFields(fields: Seq[BitField], accum: Seq[String], offset: Int): Seq[String] = fields match {
      case Nil => accum.reverse
      case bitField :: tail =>
        val generated = BitFieldGenerator(bitField, offset, field.name).generate
        recGenerateFields(tail, generated +: accum, offset + bitField.width)
    }
    recGenerateFields(bitFields, Nil, 0)
  }

}
