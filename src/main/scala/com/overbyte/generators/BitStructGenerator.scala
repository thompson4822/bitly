package com.overbyte.generators

import com.overbyte.models.{BitField, BitStruct}

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class BitStructGenerator(bitStruct: BitStruct) extends Generator {
  override def generate: String = {
    // Bail early if the struct size is invalid
    s"""
       |  var ${bitStruct.name}: Int = _
       |${generateFields.mkString}
    """.stripMargin
  }

  protected def generateFields: Seq[String] = {
    def recGenerateFields(fields: Seq[BitField], accum: Seq[String], offset: Int): Seq[String] = fields match {
      case Nil => accum.reverse
      case field :: tail =>
        val generated = BitFieldGenerator(field, offset, bitStruct.name).generate
        recGenerateFields(tail, generated +: accum, offset + field.width)
    }
    recGenerateFields(bitStruct.fields, Nil, 0)
  }

}
