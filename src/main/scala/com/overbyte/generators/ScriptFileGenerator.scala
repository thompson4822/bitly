package com.overbyte.generators

import com.overbyte.models._

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class ScriptFileGenerator(scriptFile: ScriptFile) extends Generator {
  override def generate: String =
    s"""
       |package ${scriptFile.directory.mkString(".")}
       |
       |import scodec._
       |import scodec.bits._
       |import codecs._
       |
       |${scriptFile.classes.map(ClassTypeGenerator(_).generate).mkString("\n")}
     """.stripMargin
}
