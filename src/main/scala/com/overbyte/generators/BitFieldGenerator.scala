package com.overbyte.generators

import com.overbyte.models.{BitField, BitStruct}
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class BitFieldGenerator(bitField: BitField, offset: Int, parent: String) extends LazyLogging with Generator {
  override def generate: String = bitField.name match {
    case "unused" => ""
    case other if bitField.width == 1 =>
      if(!(bitField.name.startsWith("is") || bitField.name.startsWith("has")))
        logger.warn(s"The field '${bitField.name}' appears to be boolean, but doesn't begin with 'is' or 'has'")
      Seq(generateBooleanGetter, generateBooleanSetter).mkString
    case _ => Seq(generateGetter, generateSetter).mkString
  }

  def maskBits(size: Int): Int = {
    Integer.parseInt(Range(0, size).map(_ => "1").mkString, 2)
  }

  def maskOut(size: Int, offset: Int): Int = {
    ~(maskBits(size) << offset)
  }

  def comment: String = s"// $parent offset: $offset width: ${bitField.width}"

  def generateBooleanGetter: String =
    s"""
       |  $comment
       |  def ${bitField.name}: Boolean = ($parent ${if(offset != 0) s">>> $offset" else ""} & ${maskBits(bitField.width)}) == 1
     """.stripMargin

  def generateBooleanSetter: String =
    if (offset != 0)
      s"""
         |  $comment
         |  def ${bitField.name}_=(flag: Boolean): Unit = {
         |    val flagBit = if(flag) 1 else 0
         |    $parent = $parent & ${maskOut(bitField.width, offset)} | (flagBit << $offset)
         |  }
       """.stripMargin
    else
      s"""
         |  $comment
         |  def ${bitField.name}_=(flag: Boolean): Unit = {
         |    val flagBit = if(flag) 1 else 0
         |    $parent = $parent & ${maskOut(bitField.width, 0)} | flagBit
         |  }
       """.stripMargin


  def generateGetter: String =
    s"""
       |  $comment
       |  def ${bitField.name}: Int = $parent ${if(offset != 0) s">>> $offset" else ""} & ${maskBits(bitField.width)}
     """.stripMargin

  def generateSetter: String = {
    if (offset != 0)
      s"""
         |  $comment
         |  def ${bitField.name}_=(value: Int): Unit = {
         |    require (value >= 0 && value <= ${maskBits(bitField.width)})
         |    $parent = $parent & ${maskOut(bitField.width, offset)} | (value << $offset)
         |  }
       """.stripMargin
    else
      s"""
         |  $comment
         |  def ${bitField.name}_=(value: Int): Unit = {
         |    require (value >= 0 && value <= ${maskBits(bitField.width)})
         |    $parent = $parent & ${maskOut(bitField.width, offset)} | value
         |  }
       """.stripMargin
  }

}
