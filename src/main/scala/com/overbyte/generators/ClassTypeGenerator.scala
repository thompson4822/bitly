package com.overbyte.generators

import com.overbyte.models.{ClassType, Field, NumericField, StringField}

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class ClassTypeGenerator(classType: ClassType) extends Generator {
  def codecTypeFor(field: Field): String = {
    val numericMapping = Map(
      "Long" -> "int64L",
      "Double" -> "doubleL",
      "Float" -> "floatL",
      "Int" -> "int32L",
      "Short" -> "short16L",
      "Byte" -> "byte"
    )
    field match {
      case f: StringField =>
        s"fixedSizeBits(${f.width*8}L, utf8)"
      case f: NumericField =>
        numericMapping(f.fieldType)
    }
  }
  override def generate: String = {
    val fields = classType.fields
    val fieldNames = fields.map(_.name)
    val className = classType.name
    val parameters = fields.map(field => s"var ${field.name}: ${field.fieldType}").mkString(", ")
    s"""
       |case class $className($parameters) {
       |${fields.map(FieldGenerator(_).generate).mkString}
       |}
       |
       |object $className {
       |  private val internalCodec: Codec[${fields.map(_.fieldType).mkString(" ~ ")}] =
       |    ${fields.map(codecTypeFor).mkString(" ~ ")}
       |
       |  private val codec: Codec[$className] = internalCodec.xmap[$className](
       |    {
       |      case ${fieldNames.mkString(" ~ ")} =>
       |        $className(${fieldNames.mkString(", ")})
       |    },
       |    {
       |      value =>
       |        ${fieldNames.map(name => s"value.$name").mkString(" ~ ")}
       |    }
       |  )
       |
       |  def encoded(instance: ${className}): BitVector = codec.encode(instance).require
       |  def decoded(bits: BitVector): ${className} = codec.decode(bits).require.value
       |}
     """.stripMargin
  }
}
