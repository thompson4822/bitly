package com.overbyte.generators

import com.overbyte.models._

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class ScriptFileTestGenerator(scriptFile: ScriptFile) extends Generator {
  override def generate: String =
    s"""
       |package ${scriptFile.directory.mkString(".")}
       |
       |import org.scalacheck.{Gen, Properties}
       |import org.scalacheck.Prop._
       |
       |${scriptFile.classes.map(ClassTypeTestGenerator(_).generate).mkString("\n")}
     """.stripMargin
}
