package com.overbyte.generators

/**
  * Created by steve at OverByte on 6/25/16.
  */
trait Generator { def generate: String }
