package com.overbyte.generators

import com.overbyte.models.{Field, NumericField, StringField}

/**
  * Created by steve at OverByte on 7/9/16.
  */
case class FieldGenerator(field: Field) extends Generator {
  override def generate: String = field match {
    case f: NumericField =>
      NumericFieldGenerator(f).generate
    case f: StringField => ""
  }

}
