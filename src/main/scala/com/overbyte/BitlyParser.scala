package com.overbyte

import com.overbyte.models._

import scala.util.parsing.combinator.RegexParsers

/**
  * Created by steve at OverByte on 6/25/16.
  */
class BitlyParser extends RegexParsers {
  val ident = """([a-zA-Z][a-zA-Z0-9_]*)""".r
  lazy val script: Parser[ScriptFile] = namespace ~ rep(classType) ^^ {
    case directory ~ classes => ScriptFile(directory, classes)
  }

  lazy val namespace: Parser[Seq[String]] = "package" ~> repsep(ident, ".") ^^ {
    case pieces => pieces
  }

  lazy val classType: Parser[ClassType] =
    ("class" ~> ident) ~ ("{" ~> rep(field) <~ "}") ^^ {
      case name ~ fields => ClassType(name, fields)
    }

  lazy val field: Parser[Field] = (numericField | stringField) ^^ {
    case field => field
  }

  lazy val stringField: Parser[StringField] = (ident <~ ":") ~ ("String" ~> number) ^^ {
    case name ~ size => StringField(name, size)
  }

  lazy val numericField: Parser[NumericField] = (ident <~ ":") ~ ("Long" | "Int" | "Double" | "Float" | "Short" | "Byte") ~ opt(bitStruct) ^^ {
    case name ~ fieldType ~ bitFields => NumericField(name, fieldType, bitFields)
  }


  lazy val bitStruct: Parser[Seq[BitField]] = ("{" ~> rep(bitField) <~ "}") ^^ {
    case fields => fields
  }

  lazy val bitField: Parser[BitField] = (ident <~ ":") ~ number ^^ {
    case name ~ length => BitField(name, length)
  }

  lazy val number: Parser[Int] = """(0|[1-9]\d*)""".r ^^ { _.toInt }

}
