package com.overbyte.models;

/**
 * Created by steve at OverByte on 7/9/16.
 */
trait Field {
    def name: String
    def fieldType: String
}
