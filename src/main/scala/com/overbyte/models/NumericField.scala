package com.overbyte.models

/**
  * Created by steve at OverByte on 7/2/16.
  */
case class NumericField(name: String, fieldType: String, bitFields: Option[Seq[BitField]]) extends Field
