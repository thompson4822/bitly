package com.overbyte.models

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class BitStruct(name: String, fields: Seq[BitField]) {
  require(totalFieldLength <= 32)

  protected def totalFieldLength: Int = fields.map(_.width).sum
}
