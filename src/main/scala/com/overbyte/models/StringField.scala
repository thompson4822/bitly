package com.overbyte.models

/**
  * Created by steve at OverByte on 7/9/16.
  */
case class StringField(name: String, width: Int) extends Field {
  override def fieldType: String = "String"
}
