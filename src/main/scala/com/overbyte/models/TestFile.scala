package com.overbyte.models

import com.overbyte.generators.Generator

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class TestFile(directory: Seq[String], classes: Seq[ClassType]) extends Generator {
  override def generate: String = ???
}
