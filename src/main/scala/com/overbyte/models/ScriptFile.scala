package com.overbyte.models

/**
  * Created by steve at OverByte on 6/25/16.
  */
case class ScriptFile(directory: Seq[String], classes: Seq[ClassType])
