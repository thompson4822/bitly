package com.overbyte

import java.io.File
import java.nio.file.{Files, Paths}

import com.overbyte.models.ScriptFile

import scala.io.Source
import generators._

object Main {
  // This has to generate the actual class source
  def generate(file: ScriptFile): String = {
    ScriptFileGenerator(file).generate
  }

  def getListOfFiles(dir: String, extensions: List[String]): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.filter { file =>
        extensions.exists(file.getName.endsWith(_))
      }
    }
    else {
      List[File]()
    }
  }

  def showUsage: Unit = {
    println("--**> USAGE <**--")
    println("bitly <Path to *.bit definitions> <Project path>")
  }

  def saveToFile(path: String, filename: String, source: String): Unit = {
    Files.createDirectories(Paths.get(path))
    Files.write(Paths.get(Seq(path, filename + ".scala").mkString("/")), source.getBytes)
  }

  def saveToSourceFile(projectPath: String, namespace: Seq[String], filename: String, scriptFile: ScriptFile): Unit = {
    val fullPath: String = (Seq(projectPath, "src", "main", "scala") ++: namespace).mkString("/")
    saveToFile(fullPath, filename, ScriptFileGenerator(scriptFile).generate)
  }

  def saveToTestFile(projectPath: String, namespace: Seq[String], filename: String, scriptFile: ScriptFile): Unit = {
    val fullPath: String = (Seq(projectPath, "src", "test", "scala") ++: namespace).mkString("/")
    saveToFile(fullPath, filename + "Specification", ScriptFileTestGenerator(scriptFile).generate)
  }

  def main(args: Array[String]) {
    if (args.length < 2) {
      showUsage
    }
    else {
      val sourcePath = args(0)
      val outputPath = args(1)
      val files = getListOfFiles(sourcePath, List("bit"))
      val parser = new BitlyParser
      files.foreach { file =>
        val filename = file.getName.split('.').head                       // MyFile.bit becomes MyFile.scala
        val fileContent = Source.fromFile(file).getLines.mkString("\n")   // Read MyFile.bit
        val result = parser.parseAll(parser.script, fileContent)          // Parse it
        if (result.successful) {
          val currentSource: ScriptFile = result.get                                  // Get the parsed content
//          println(currentSource)
          saveToSourceFile(outputPath, currentSource.directory, filename, currentSource) // Convert to string (source) and save to file
          saveToTestFile(outputPath, currentSource.directory, filename, currentSource)
        }
        println("All files successfully read and generated")
      }
    }
  }
}















