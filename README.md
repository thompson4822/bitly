# README #

### Purpose ###

This tool is designed expressly for representing C/C++ style bitfields in Java. After hunting for something similar, I was surprised to find that this kind of thing isn't available to the Java world.