
name := "bitly"

version := "1.3.7"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.3",
  "org.scodec" % "scodec-core_2.11" % "1.10.1",
  "ch.qos.logback" %  "logback-classic" % "1.1.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",

  "org.scalacheck" % "scalacheck_2.11" % "1.13.1" % "test",
  "org.scalatest" % "scalatest_2.11" % "3.0.0-M16-SNAP6" % "test"
)
